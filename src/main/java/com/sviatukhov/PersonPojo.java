package com.sviatukhov;

import lombok.Data;

@Data
public class PersonPojo {
    private String name;
    private String surName;
    private String patronymic;
    private int age;

    public PersonPojo(String name, String surName, String patronymic, int age) {
        this.name = name;
        this.surName = surName;
        this.patronymic = patronymic;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getFullData() {
        String fullName = "Surname: " + getSurName() +
                " Name: " + getName() +
                " Patronymic: " + getPatronymic() +
                " Age: " + getAge();
        System.out.println(fullName);
        return fullName;
    }
}
