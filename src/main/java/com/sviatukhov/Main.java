package com.sviatukhov;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/*
  Необходимо реализовать собственную обобщенную коллекцию с возможность указать при создании коллекции операции,
  в виде лямбда выражений, которые должны быть выполнены при добавлении
  (необходимо выполнить действия результатом которых будет такого же типа
  как и входящий параметр, вспоминаем метод .map() Stream API)
  и удалении элемента (выполнить действия у которых нет результата в виде возвращаемого значения [void] ).
  + Созданная коллекция должна реализовывать интерфейс java.util.Collection.
  Необходимо предоставить пример работы коллекции в виде junit теста в рамках maven проекта.
  + В качестве элементов коллекции желательно использовать свой класс, выполненный как POJO.
 */

public class Main {

    public static void main(String[] args) {
        PersonPojo person1 = new PersonPojo("Ivan", "Urgant", "Ivanovich", 30);
        PersonPojo person2 = new PersonPojo("Vasiliy", "Utkin", "Fedorovich", 55);
        PersonPojo person3 = new PersonPojo("Yuliy", "Cesar", "Dmitrievich", 40);
        PersonPojo person4 = new PersonPojo("Dmitriy", "Sviatukhov", "Ivanovich", 29);

        Collection<PersonPojo> collection = new Collection<PersonPojo>();
        collection.add(person1);
        collection.add(person2);
        collection.add(person3);
        collection.add(person4);
        List<String> result = new ArrayList<>();

        result = collection
                .stream()
                .map(p -> p.getFullData())
                //.filter(p -> p != null && !p.isEmpty())
                .collect(Collectors.toList());



        System.out.println(person4.getFullData());
    }
}
